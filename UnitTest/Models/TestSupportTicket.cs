﻿using System;

namespace UnitTest.Models
{
    public class TestSupportTicket : Domain.SupportTicket
    {
        public TestSupportTicket(string title, string body, DateTime dueTime) : base(title, body, dueTime)
        {
        }
        public TimeSpan TimeUntilOverdue => DueTime - DateTime.UtcNow;

        public bool IsCritical => TimeUntilOverdue.TotalMinutes < 60;
    }
}
