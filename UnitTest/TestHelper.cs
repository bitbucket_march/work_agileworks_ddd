﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Domain;
using UnitTest.Models;

namespace UnitTest
{
    public class TestHelper
    {


        private AppDbContext _testDbContext;
        private readonly DbContextOptions _options;

        public TestHelper()
        {
            var serviceProvider = new ServiceCollection()
                .AddEntityFrameworkInMemoryDatabase()
                .BuildServiceProvider();
            _options = new DbContextOptionsBuilder<AppDbContext>()
                .UseInMemoryDatabase(databaseName: Guid.NewGuid().ToString())
                .UseInternalServiceProvider(serviceProvider)
                .Options;
        }

        // Init helpers
        public AppDbContext GetNewDbContext()
        {
            _testDbContext = new AppDbContext(_options);
            return _testDbContext;
        }

        public void SeedDatabase()
        {
            SeedDatabase(GetTestTicketList());
        }
        public void SeedDatabase(List<TestSupportTicket> ticketsToDb)
        {
            ticketsToDb.ForEach(x => _testDbContext.Tickets.Add(x));
            _testDbContext.SaveChanges();
        }

        // As in GetVisibleTicketsQuery
        public Task<List<SupportTicket>> GetAllVisibleTicketsAsync()
        {
            return _testDbContext.Tickets
                .Where(w => w.IsVisible)
                .OrderBy(o => o.DueTime)
                .ToListAsync();
        }

        
        // Seed Data
        public List<TestSupportTicket> GetTestTicketList()
        {
            var entityList = new List<TestSupportTicket>
            {
                new TestSupportTicket(
                    "Due in hour",
                    "Critical",
                    DateTime.Now.AddMinutes(59)
                ),
                new TestSupportTicket(
                    "1 min over deadline",
                    "Critical",
                    DateTime.Now.AddMinutes(-1)
                ),
                new TestSupportTicket(
                    "Due in 3 hrs",
                    "Not critical",
                    DateTime.Now.AddHours(3)
                ),
                new TestSupportTicket(
                    "Due in 2 hrs",
                    "Not critical",
                    DateTime.Now.AddHours(2)
                ),
                new TestSupportTicket(
                    "Due in a year",
                    "Not critical",
                    DateTime.Now.AddYears(1)
                )
            };

            return entityList;
        }
        public List<TestSupportTicket> GetLongTestTicketList()
        {
            var entityList = new List<TestSupportTicket>
            {
                new TestSupportTicket(
                    "Due in 2 years",
                    "Not critical",
                    DateTime.Now.AddYears(2)
                ),
                new TestSupportTicket(
                    "Due in 30 minutes",
                    "Critical",
                    DateTime.Now.AddMinutes(30)
                ),
                new TestSupportTicket(
                    "Due in 12 hours",
                    "Not critical",
                    DateTime.Now.AddHours(12)
                ),
                new TestSupportTicket(
                    "Due in 1 minute",
                    "Critical",
                    DateTime.Now.AddMinutes(1)
                ),
                new TestSupportTicket(
                    "Overdue a day",
                    "Critical",
                    DateTime.Now.AddDays(-1)
                ),
                new TestSupportTicket(
                    "Added this moment",
                    "Critical",
                    DateTime.Now
                )
            };
            return entityList.Concat(GetTestTicketList()).ToList();
        }

        public List<TestSupportTicket> GetClosedTicketsList(int numberOfCriticalsToClose, int numberOfRegularsToClose)
        {
            return GetClosedTicketsList(numberOfCriticalsToClose, numberOfRegularsToClose, GetLongTestTicketList());
        }
        public List<TestSupportTicket> GetClosedTicketsList(int numberOfCriticalsToClose, int numberOfRegularsToClose,
            List<TestSupportTicket> inputList)
        {
            inputList.Where(x => x.IsCritical && x.ClosedTime == null)
                    .Take(numberOfCriticalsToClose)
                    .ToList()
                    .ForEach(y => y.Close());
            inputList.Where(x => !x.IsCritical && x.ClosedTime == null)
                .Take(numberOfRegularsToClose)
                .ToList()
                .ForEach(y => y.Close());

            return inputList;
        }
    }
}
