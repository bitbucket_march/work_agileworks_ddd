﻿using System;
using System.Collections.Generic;
using System.Linq;
using Domain.Specs;
using Shouldly;
using UnitTest.Models;
using Xunit;

namespace UnitTest
{
    public class SpecificationTests
    {
        private readonly TestHelper _testHelper;
        public SpecificationTests()
        {
            _testHelper = new TestHelper();
        }
        [Theory]
        [InlineData(true, 3)]
        [InlineData(false, 2)]
        public void TicketVisibilitySpec_Should_Satisfy_On_Argument(bool visible, int expectedSatisfiedCount)
        {
            // Init
            var ticketVisibilitySpec = new TicketVisibilitySpec(visible);
            var initialList = _testHelper.GetTestTicketList();
            var satisfiedList = new List<bool>();

            // Close 2 first tickets
            var closedTickets = initialList.Take(2).ToList();
            closedTickets.ForEach(x => x.Close());
            closedTickets.AddRange(initialList.TakeLast(3));

            // Act
            foreach (var ticket in closedTickets)
            {
                satisfiedList.Add(ticketVisibilitySpec.IsSatisfiedBy(ticket));
            }

            // Assert
            satisfiedList.Count(x => x).ShouldBe(expectedSatisfiedCount);
            satisfiedList.Take(2).ToList().ForEach(x => x.ShouldBe(!visible));

        }

        [Theory]
        // days, hours, minutes, seconds, expectedTicketsCount
        [InlineData(0, 1, 0, 0, 2)]
        [InlineData(0, 3, 0, 0, 4)]
        [InlineData(0, 0, 0, 1, 1)]
        [InlineData(366, 0, 0, 0, 5)]
        public void TicketCriticalSpec_Should_Satisfy_On_Argument(
            int days, int hours, int minutes, int seconds, int expectedTicketsCount)
        {
            // Init
            var targetTime = new TimeSpan(days,hours, minutes, seconds);
            var ticketCriticalSpec = new TicketOverdueInGivenTimeSpec(targetTime);
            var ticketsList = _testHelper.GetTestTicketList();
            var satisfiedList = new List<TestSupportTicket>();

            // Act
            foreach (var ticket in ticketsList)
            {
                if (ticketCriticalSpec.IsSatisfiedBy(ticket))
                {
                    satisfiedList.Add(ticket);
                }
            }
            
            //Assert
            satisfiedList.Count().ShouldBe(expectedTicketsCount);
            satisfiedList.All(x => x.TimeUntilOverdue <= targetTime).ShouldBe(true);
        }

        [Theory]
        // Visible, nr of criticals to close, nr of non-criticals to close, day, hr, min, sec, expected results
        [InlineData(true, 4, 2, 0, 1, 0 , 0, 2)]
        [InlineData(true, 6, 0 , 0, 13, 0, 0, 3)]
        [InlineData(true, 6, 5, 366, 0, 0, 0, 0)]
        [InlineData(false, 6, 5, 366, 0, 0, 0, 10)]
        [InlineData(false, 3, 3, 0, 13, 0, 0, 5)]
        public void AndSpec_Combines_Two_Specs_And_Satisfies(
            bool visible, 
            int numberOfCriticalsToClose, int numberOfRegularsToClose,
            int days, int hours, int minutes, int seconds,
            int expectedTicketsCount)
        {
            // Init
            var targetTime = new TimeSpan(days, hours, minutes, seconds);
            var ticketVisibilitySpec = new TicketVisibilitySpec(visible);
            var ticketCriticalSpec = new TicketOverdueInGivenTimeSpec(targetTime);

            var combinedSpec = ticketVisibilitySpec & ticketCriticalSpec;
            
            var satisfiedList = new List<TestSupportTicket>();

            // Get a list of random closed tickets
            var ticketsList = _testHelper.GetClosedTicketsList(numberOfCriticalsToClose, numberOfRegularsToClose);
            System.Diagnostics.Debugger.Launch();
            foreach (var ticket in ticketsList)
            {
                if (combinedSpec.IsSatisfiedBy(ticket))
                {
                    satisfiedList.Add(ticket);
                }
            }

            satisfiedList.Count.ShouldBe(expectedTicketsCount);
        }
    }
}
