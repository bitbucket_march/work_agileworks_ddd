﻿using System;
using System.Threading;
using System.Threading.Tasks;
using CSupportWebApp.Commands;
using CSupportWebApp.Queries;
using Shouldly;
using UnitTest.Commands;
using Xunit;

namespace UnitTest
{
    public class MediatRTests
    {
        private readonly TestHelper _testHelper;
        public MediatRTests()
        {
            _testHelper = new TestHelper();
        }
        
        [Fact]
        public async Task GetVisibleTicketsQuery_Returns()
        {
            var dbContext = _testHelper.GetNewDbContext();

            var getVisibleTicketsQuery = new GetVisibleTicketsQuery();
            var handler = new GetVisibleTicketsQuery.Handler(dbContext);

            _testHelper.SeedDatabase();

            // Act
            var tickets = await handler.Handle(getVisibleTicketsQuery, new CancellationToken(false));

            // Assert
            tickets.Count.ShouldBe(5);
            
        }

        [Fact]
        public async Task CreateTicketCommand_AddsToDatabase()
        {
            var dbContext = _testHelper.GetNewDbContext();

            var createTicketCommand = new CreateTicketCommand
            {
                Title = "Due in hour",
                Body = "Should be red in UI",
                DueTime = DateTime.Now.AddMinutes(59)
            };
            var handler = new TestCreateTicketCommandHandler(dbContext);

            // Act
            await handler.Handle(createTicketCommand);

            // Assert
            var tickets = await _testHelper.GetAllVisibleTicketsAsync();
            tickets.Count.ShouldBe(1);
        }

        [Fact]
        public async Task CloseTicketCommand_DoesNotShowAsVisible()
        {
            var dbContext = _testHelper.GetNewDbContext();

            _testHelper.SeedDatabase();
            
            // Pass random ticketId to command
            int randomId = new Random().Next(1, 6);
            var command = new CloseTicketCommand{Id = randomId};
            var handler = new TestCloseTicketCommandHandler(dbContext);

            // Act
            await handler.Handle(command);

            // Get visible tickets with query
            var query = new GetVisibleTicketsQuery();
            var queryHandler = new GetVisibleTicketsQuery.Handler(dbContext);

            var tickets = await queryHandler.Handle(query, new CancellationToken(false));

            // Assert
            tickets.Count.ShouldBe(4);
            tickets.ForEach(a => a.Id.ShouldNotBe(randomId));
        }

    }
}
