﻿using Shouldly;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Xunit;

namespace UnitTest
{
    public class DbUnitTests
    {
        private readonly TestHelper _testHelper;
        public DbUnitTests()
        {
            _testHelper = new TestHelper();
        }

        // ctx Adds 5 tickets, gets 5 tickets
        [Fact]
        public async Task DbContext_Add_And_Save_Creates_5Tickets_In_DB()
        {
            var dbContext = _testHelper.GetNewDbContext();
            var ticketList = _testHelper.GetTestTicketList();
            
            // Act
            ticketList.ForEach(x => dbContext.Tickets.Add(x));
            dbContext.SaveChanges();
            var ticketsFromDb = await dbContext.Tickets.ToListAsync();

            // Assert
            ticketsFromDb.Count.ShouldBe(5);
        }

        // ctx Adds 5 tickets, returns 4 visible tickets ordered by deadline
        [Fact]
        public async Task DbContext_Should_Get_Only_Visible_Tickets_And_Order_By_Deadline()
        {
            var dbContext = _testHelper.GetNewDbContext();
            var ticketList = _testHelper.GetTestTicketList();
            ticketList.Reverse();
            ticketList[0].Close();

            //For later comparison we create a correctly sorted list missing the closed ticket
            var correctList = ticketList;
            correctList.RemoveAt(0);
            correctList = correctList.OrderBy(o => o.DueTime).ToList();

            // Act
            ticketList.ForEach(x => dbContext.Tickets.Add(x));
            dbContext.SaveChanges();

            // Assert
            // Note! GetAllVisibleTicketsAsync is not used anymore.
            // GetVisibleTicketsQuery is used instead which uses specifications.
            // That query's functionality is tested in MediatRTests
            var ticketsFromDb = await _testHelper.GetAllVisibleTicketsAsync();
            ticketsFromDb.Count.ShouldBe(4);
            for (var i = 0; i < ticketsFromDb.Count; i++)
            {
                ticketsFromDb[i].Title.ShouldBe(correctList[i].Title);
            }
        }
    }
}
