﻿using System;
using Shouldly;
using Xunit;
using Domain;

namespace UnitTest
{
    public class SupportTicketModelTests
    {
        [Fact]
        public void SupportTicket_Should_Create_A_Valid_Ticket()
        {
            // Act and Assert
            Should.NotThrow(() =>
            {
                var ticket = new SupportTicket(
                "Due in hour",
                "RED",
                DateTime.Now.AddMinutes(60));

                ticket.IsVisible.ShouldBeTrue();
            });
        }
        
        [Fact]
        public void SupportTicket_Should_Set_Invisible_On_Close()
        {
            var ticket = new SupportTicket(
                "Due in hour",
                "RED",
                DateTime.Now.AddMinutes(60));

            // Act
            ticket.Close();

            // Assert
            ticket.ClosedTime?.ShouldBeLessThan(DateTime.UtcNow);
            ticket.IsVisible.ShouldBeFalse();
        }
        [Fact]
        public void SupportTicket_Should_Throw_Attempting_To_Create_Invalid_Ticket()
        {
            Should.Throw<ArgumentException>(() =>
                new SupportTicket(
                    "Invalid body",
                    "",
                    DateTime.Now.AddMinutes(59)
            ));

            Should.Throw<ArgumentException>(() =>
                new SupportTicket(
                    "",
                    "Invalid title",
                    DateTime.Now.AddMinutes(59)
            ));
        }
    }
}
