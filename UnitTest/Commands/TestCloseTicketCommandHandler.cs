﻿using System.Threading.Tasks;
using CSupportWebApp.Commands;
using Domain;

namespace UnitTest.Commands
{
    public class TestCloseTicketCommandHandler : CloseTicketCommand.Handler
    {
        public TestCloseTicketCommandHandler(AppDbContext dbContext) : base(dbContext)
        {
        }

        public async Task Handle(CloseTicketCommand request)
        {
            await base.Handle(request, default);
        }
    }
}
