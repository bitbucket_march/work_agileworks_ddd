﻿using System.Threading.Tasks;
using CSupportWebApp.Commands;
using Domain;

namespace UnitTest.Commands
{
    public class TestCreateTicketCommandHandler : CreateTicketCommand.Handler
    {
        public TestCreateTicketCommandHandler(AppDbContext dbContext) : base(dbContext)
        {
        }

        public async Task Handle(CreateTicketCommand request)
        {
            await base.Handle(request, default);
        }
    }
}
