﻿using System;
using System.Threading.Tasks;
using Xunit;
using Shouldly;
using System.Collections.Generic;
using System.Linq;
using CSupportWebApp.Models;
using Microsoft.AspNetCore.Mvc;
using Domain;

namespace UnitTest
{
    public class WebAppTests
    {
        //TestHelper testHelper;
        //public WebAppTests()
        //{
        //    testHelper = new TestHelper();
        //}

        ////Controller returns 5 previously added tickets to view and shows correct critical status
        //[Fact]
        //public async Task ControllerReturnsTicketsInIndexViewWithCorrectCriticalStatus()
        //{
        //    // Seed Database
        //    var testBLL = testHelper.GetNewTestUOW();
        //    foreach (var ticket in getTestControllerTicketList())
        //    {
        //        await testBLL.Tickets.AddAsync(ticket);
        //    }
        //    await testBLL.SaveChangesAsync();

        //    // Query Index
        //    var testTicketController = testHelper.GetNewTestTicketsController();
        //    var result = await testTicketController.Index() as ViewResult;

        //    // Ensure 5 tickets in result
        //    var resultData = (List<SupportTicket>)result.ViewData.Model;
        //    resultData.Count.ShouldBe(5);

        //    // IF in less than hour due, then is critical.
        //    //foreach (var resultEntity in resultData)
        //    //{
        //    //    if (resultEntity.DueTime < DateTime.Now || (resultEntity.DueTime - DateTime.Now).TotalHours < 1)
        //    //    {
        //    //        resultEntity.IsCritical.ShouldBeTrue();
        //    //    }
        //    //    else
        //    //    {
        //    //        resultEntity.IsCritical.ShouldBeFalse();
        //    //    }
        //    //}
        //}

        //// Controller adds a ticket to repository and returns to Index view.
        //[Fact]
        //public async Task ControllerAddsSingleTicketAndReturnsToIndex()
        //{
        //    var validTicket = new TicketVM(
        //        "Due in hour",
        //        "char",
        //        DateTime.Now.AddHours(1)
        //    );

        //    var testTicketController = testHelper.GetNewTestTicketsController();

        //    // Creating a ticket
        //    var actionResultTask = testTicketController.Create(validTicket);
        //    actionResultTask.Wait();

        //    // Returns to Index() ?
        //    var redirectResult = actionResultTask.Result as RedirectToActionResult;
        //    redirectResult.ActionName.ShouldBe("Index");

        //    // Ensure ticket in DB
        //    var testUOW = testHelper.GetNewTestUOW();

        //    var tickets = await testUOW.Tickets.GetAllVisibleTicketsAsync();
        //    tickets.Count.ShouldBe(1);
        //}

        // Controller adds 5 tickets, closes one and returns to Index view.
        //[Fact]
        //public async Task ControllerClosesRandomTicketAndReturnsToIndex()
        //{
        //    var testList = getTestControllerTicketList();

        //    var testTicketController = testHelper.GetNewTestTicketsController();
        //    // Add 5 tickets
        //    foreach (var ticket in testList)
        //    {
        //        await testTicketController.Create( new TicketVM(
        //            ticket.Title,
        //            ticket.Body,
        //            ticket.DueTime));
        //    }

        //    // Close random ticket
        //    int randomId = new Random().Next(1, 6);
        //    testTicketController = testHelper.GetNewTestTicketsController();
        //    var actionResultTask = testTicketController.CloseTicketHandler(randomId);
        //    actionResultTask.Wait();

        //    // Ensure random ticket closed
        //    var testBLL = testHelper.GetNewTestUOW();
        //    var resultSet = await testBLL.Tickets.GetAllVisibleTicketsAsync();
        //    resultSet.Count.ShouldBe(4);
        //    resultSet.Any(o => o.Id == randomId).ShouldBeFalse();

        //    // Returned to Index() ?
        //    var redirectResult = actionResultTask.Result as RedirectToActionResult;
        //    redirectResult.ActionName.ShouldBe("Index");
        //}


        
    }
}
