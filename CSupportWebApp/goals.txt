﻿Ülesanne on teostada lihtne veebirakendus, mis võimaldaks hallata kasutajatoele saadetud pöördumisi. Lihtsustatud süsteemi funktsionaalsus oleks järgmine:
•         Kasutaja saab sisestada pöördumise.
•         Pöördumisel peab olema kirjeldus, sisestamise aeg, lahendamise tähtaeg. Sisestamise ajaks märgitakse pöördumise sisestamise aeg, teised kohustuslikud väljad täidab kasutaja.
•         Kasutajale kuvatakse aktiivsed pöördumised koos kõigi väljadega nimekirjas sorteeritult kahanevalt lahendamise tähtaja järgi.
•         Pöördumised, mille lahendamise tähtajani on jäänud vähem kui 1 tund või mis on juba ületanud lahendamise tähtaja, kuvatakse nimekirjas punasena.
•         Kasutaja saab nimekirjas pöördumisi lahendatuks märkida, mis kaotab pöördumise nimekirjast.
Võib eeldada modernse brauseri olemasolu (HTML5 jne). Andmeid ei ole vaja andmebaasi salvestada, võib vabalt hoida neid ka mälus. Sooviksime töös näha kindlasti ka üksusteste.
Kui tekib küsimusi, siis kirjuta julgelt!