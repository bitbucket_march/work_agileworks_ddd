using System;
using System.Threading.Tasks;
using CSupportWebApp.Commands;
using CSupportWebApp.Models;
using CSupportWebApp.Queries;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace CSupportWebApp.Controllers
{
    public class TicketsController : Controller
    {
        //private readonly ITicketCommandService _commands;
        //private readonly ITicketQueryService _queries;
        private readonly IMediator _mediator;

        public TicketsController(IMediator mediator)
        {
            _mediator = mediator;
        }

        // GET: Tickets
        public async Task<IActionResult> Index()
        {
            return View(await _mediator.Send(new GetVisibleTicketsQuery()));

            // return View(await _mediator.Send(new GetVisibleTicketsQuery { OnlyCritical = true }));
        }


        // GET: Tickets/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Tickets/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([FromForm] TicketVM ticketVM)
        {
            if (ModelState.IsValid)
            {
                await _mediator.Send(new CreateTicketCommand
                {
                    Title = ticketVM.Title,
                    Body = ticketVM.Body,
                    DueTime = ticketVM.DueTime
                });
                return RedirectToAction(nameof(Index));
            }
            return View(ticketVM);
        }

        public async Task<IActionResult> CloseTicket(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            try
            {
                await _mediator.Send(new CloseTicketCommand { Id = (int)id });
            }
            catch (InvalidOperationException)
            {
                return NotFound();
            }
            
            return RedirectToAction(nameof(Index));
        }
    }
}
