﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CSupportWebApp.Models
{
    public class TicketVM : IValidatableObject
    {

        public TicketVM(string title, string body, DateTime dueTime)
        { 
            Title = title;
            Body = body;
            DueTime = dueTime;
        }

        public TicketVM() { }

        [Required]
        [MinLength(1)]
        [MaxLength(64)]
        public string Title { get; set; }
        [MinLength(1)]
        [MaxLength(1024)]
        public string Body { get; set; }
        [Required]
        public DateTime DueTime { get; set;}

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            if (DueTime.ToUniversalTime() < DateTime.UtcNow)
            {
                yield return new ValidationResult(
                
                    "Can't add overdue ticket.", 
                    new [] {"DueTime"});
            }
        }
    }
}
