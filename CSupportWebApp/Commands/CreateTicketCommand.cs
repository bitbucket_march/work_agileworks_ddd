﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Domain;
using MediatR;

namespace CSupportWebApp.Commands
{
    public class CreateTicketCommand : IRequest
    {
        public string Title { get; set; }
        public string Body { get; set; }
        public DateTime DueTime { get; set; }

        public class Handler : AsyncRequestHandler<CreateTicketCommand>
        {
            private readonly AppDbContext _dbContext;

            public Handler(AppDbContext dbContext)
            {
                _dbContext = dbContext;
            }

            protected override async Task Handle(CreateTicketCommand request, CancellationToken cancellationToken)
            {
                var ticket = new SupportTicket(request.Title, request.Body, request.DueTime.ToUniversalTime());
                await _dbContext.Tickets.AddAsync(ticket);
                await _dbContext.SaveChangesAsync();
            }
        }

    }
}
