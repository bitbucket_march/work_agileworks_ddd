﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Domain;
using MediatR;

namespace CSupportWebApp.Commands
{
    public class CloseTicketCommand : IRequest
    {
        public int Id { get; set; }

        public class Handler : AsyncRequestHandler<CloseTicketCommand>
        {
            private readonly AppDbContext _dbContext;

            public Handler(AppDbContext dbContext)
            {
                _dbContext = dbContext;
            }

            protected override async Task Handle(CloseTicketCommand request, CancellationToken cancellationToken)
            {
                var ticket = await _dbContext.Tickets.FindAsync(request.Id);
                if (ticket == null)
                {
                    throw new InvalidOperationException("Ticket does not exist");
                }
                ticket.Close();
                _dbContext.Tickets.Update(ticket);
                await _dbContext.SaveChangesAsync();
            }
        }
    }


}
