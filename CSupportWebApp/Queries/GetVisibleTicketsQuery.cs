﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Domain;
using Domain.Specs;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace CSupportWebApp.Queries
{
    public class GetVisibleTicketsQuery : IRequest<List<SupportTicket>>
    {
        public bool OnlyCritical { get; set; } = false;

        public class Handler : IRequestHandler<GetVisibleTicketsQuery, List<SupportTicket>>
        {
            private readonly AppDbContext _dbContext;
            public Handler(AppDbContext dbContext)
            {
                _dbContext = dbContext;
            }

            public async Task<List<SupportTicket>> Handle(GetVisibleTicketsQuery message, CancellationToken cancellationToken)
            {
                var ticketVisibilitySpec = new TicketVisibilitySpec();
                var ticketTargetTimeSpec =
                    new TicketOverdueInGivenTimeSpec(new TimeSpan(1, 0, 0));

                if (message.OnlyCritical)
                {
                    return await _dbContext.Tickets
                        .Where((ticketTargetTimeSpec & ticketVisibilitySpec).ToExpression())
                        .OrderBy(o => o.DueTime)
                        .ToListAsync();
                }

                return await _dbContext.Tickets
                    .Where(ticketVisibilitySpec.ToExpression())
                    .OrderBy(o => o.DueTime)
                    .ToListAsync();

            }

        }

    }
}
