﻿using System;
using System.Linq.Expressions;
using Domain.Specs.LogicalSpecs;


namespace Domain.Specs
{
    public abstract class BaseSpec<TEntity>
    {

        public abstract Expression<Func<TEntity, bool>> ToExpression();

        public bool IsSatisfiedBy(TEntity entity)
        {
            return ToExpression().Compile().Invoke(entity);
        }

        // karelg @ EVR
        public static BaseSpec<TEntity> operator &(BaseSpec<TEntity> firstSpec, BaseSpec<TEntity> secondSpec)
        {
            return new AndSpec<TEntity>(firstSpec, secondSpec);
        }

    }
}
