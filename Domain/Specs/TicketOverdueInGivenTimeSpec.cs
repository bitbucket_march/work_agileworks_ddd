﻿using System;
using System.Linq.Expressions;
namespace Domain.Specs
{
    public class TicketOverdueInGivenTimeSpec : BaseSpec<SupportTicket>
    {
        private readonly TimeSpan _targetTime;

        public TicketOverdueInGivenTimeSpec(TimeSpan targetTime)
        {
            _targetTime = targetTime;
        }
        public override Expression<Func<SupportTicket, bool>> ToExpression()
        {
            return ticket => (ticket.DueTime - DateTime.UtcNow) <= _targetTime;
        }
    }
}
