﻿using System;
using System.Linq.Expressions;


namespace Domain.Specs
{
    public class TicketVisibilitySpec : BaseSpec<SupportTicket>
    {
        private readonly bool _visible;

        public TicketVisibilitySpec(bool visible = true)
        {
            _visible = visible;
        }

        public override Expression<Func<SupportTicket, bool>> ToExpression()
        {
            return ticket => ticket.IsVisible == _visible;
        }
    }
}
