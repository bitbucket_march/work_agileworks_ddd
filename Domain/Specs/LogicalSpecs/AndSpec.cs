﻿using System;
using System.Linq.Expressions;

namespace Domain.Specs.LogicalSpecs
{
    public class AndSpec<TEntity> : BaseSpec<TEntity>
    {
        private readonly BaseSpec<TEntity> _firstSpec;
        private readonly BaseSpec<TEntity> _secondSpec;

        public AndSpec(BaseSpec<TEntity> firstSpec, BaseSpec<TEntity> secondSpec)
        {
            _firstSpec = firstSpec;
            _secondSpec = secondSpec;
        }

        public override Expression<Func<TEntity, bool>> ToExpression()
        {
            var firstExpression = _firstSpec.ToExpression();
            var secondExpression = _secondSpec.ToExpression();

            
            var andExpression = Expression.AndAlso(
                firstExpression.Body, 
                Expression.Invoke(secondExpression, firstExpression.Parameters[0]));

            return Expression.Lambda<Func<TEntity, bool>>(
                andExpression, firstExpression.Parameters);
        }
    }
}
