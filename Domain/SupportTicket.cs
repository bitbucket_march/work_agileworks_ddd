﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Domain
{
    public class SupportTicket : BaseEntity
    {

        public SupportTicket(string title, string body, DateTime dueTime)
        {
            if (String.IsNullOrWhiteSpace(title))
            {
                throw new ArgumentException(nameof(title));
            }
            if (String.IsNullOrWhiteSpace(body))
            {
                throw new ArgumentException(nameof(body));
            }
            if (dueTime == null)
            {
                throw new ArgumentException(nameof(dueTime));
            }
            Title = title;
            Body = body;
            DueTime = dueTime.ToUniversalTime();
            TimeCreated = DateTime.UtcNow;
        }

        public SupportTicket() { }

        public void Close()
        {
            ClosedTime = DateTime.UtcNow;
        }

        [Required]
        [MinLength(1)]
        [MaxLength(64)]
        public string Title { get; private set; }
        [MinLength(1)]
        [MaxLength(1024)]
        public string Body { get; private set; }
        public DateTime TimeCreated { get; private set; }
        [Required]
        public DateTime DueTime { get; private set; }
        public DateTime? ClosedTime { get; private set; }
        public bool IsVisible => ClosedTime == null;
    }
}
