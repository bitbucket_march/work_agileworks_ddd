﻿using Domain.Interfaces;
using System.ComponentModel.DataAnnotations;

namespace Domain
{
    public abstract class BaseEntity : IBaseEntity
    {
        public int Id { get; set; }
    }
}
